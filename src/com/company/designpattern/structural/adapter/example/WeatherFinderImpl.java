package com.company.designpattern.structural.adapter.example;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public class WeatherFinderImpl implements WeatherFinder {
    @Override
    public double find(String city) {
        return 33.6;
    }
}
