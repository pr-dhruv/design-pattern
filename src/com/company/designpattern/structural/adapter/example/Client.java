package com.company.designpattern.structural.adapter.example;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public class Client {
    public static void main(String[] args) {
        WeatherUI ui = new WeatherUI();
        System.out.println(ui.showTemperature(560065));
    }
}
