package com.company.designpattern.structural.adapter.example;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public interface WeatherFinder {
    double find(String city);
}
