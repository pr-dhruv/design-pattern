package com.company.designpattern.structural.adapter.example;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public class WeatherAdapter {
    public double findTemperature(int zipCode) {
        String city = null;

        if (zipCode == 560065)
            city = "Bangalore";

        WeatherFinder weather = new WeatherFinderImpl();
        return weather.find(city);
    }
}
