package com.company.designpattern.structural.adapter.example;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public class WeatherUI {
    public double showTemperature(int zipCode) {
        WeatherAdapter adapter = new WeatherAdapter();
        return adapter.findTemperature(zipCode);
    }
}
