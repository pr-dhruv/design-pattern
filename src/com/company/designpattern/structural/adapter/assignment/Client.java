package com.company.designpattern.structural.adapter.assignment;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public class Client {
    public static void main(String[] args) {
        PaymentApp pa = new PaymentApp();
        pa.pay(7680);
    }
}
