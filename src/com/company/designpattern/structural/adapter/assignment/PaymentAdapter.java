package com.company.designpattern.structural.adapter.assignment;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public class PaymentAdapter {
    public void pay(double rupee) {
        double dollor = rupee / 74.72;

        PaymentProcessor pp = new PaymentProcessorImpl();
        pp.pay(dollor);
    }
}
