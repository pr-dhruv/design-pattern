package com.company.designpattern.structural.adapter.assignment;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public class PaymentApp {
    public void pay(double rupee) {
        PaymentAdapter pa = new PaymentAdapter();
        pa.pay(rupee);
    }
}
