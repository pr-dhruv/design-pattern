package com.company.designpattern.structural.adapter.assignment;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public class PaymentProcessorImpl implements PaymentProcessor {
    @Override
    public void pay(double dollor) {
        System.out.println("Amount paid : $" + dollor);
    }
}
