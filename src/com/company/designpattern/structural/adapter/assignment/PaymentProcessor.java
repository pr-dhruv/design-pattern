package com.company.designpattern.structural.adapter.assignment;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public interface PaymentProcessor {
    void pay(double dollor);
}
