package com.company.designpattern.structural.flyweigth.example.solution;

/**
 * @author Mahendra Prajapati
 * @since 30-07-2020
 */
public class Circle extends ShapeAdapter {

    private int radius;
    private String label;
    private String fillColor;
    private String lineColor;

    public Circle() {
        super();
        this.label = "Circle";
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public String getLabel() {
        return label;
    }

    public String getFillColor() {
        return fillColor;
    }

    public void setFillColor(String fillColor) {
        this.fillColor = fillColor;
    }

    public String getLineColor() {
        return lineColor;
    }

    public void setLineColor(String lineColor) {
        this.lineColor = lineColor;
    }

    @Override
    public void draw(int radius, String fillColor, String lineColor) {
        this.setRadius(radius);
        this.setLineColor(lineColor);
        this.setFillColor(fillColor);
        System.out.println(toString());
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + getRadius() +
                ", label='" + getLabel() + '\'' +
                ", fillColor='" + getFillColor() + '\'' +
                ", lineColor='" + getLineColor() + '\'' +
                ", hashcode='" + hashCode() + '\'' +
                '}';
    }
}
