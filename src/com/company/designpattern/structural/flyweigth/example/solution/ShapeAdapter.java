package com.company.designpattern.structural.flyweigth.example.solution;

/**
 * @author Mahendra Prajapati
 * @since 31-07-2020
 */
public class ShapeAdapter implements Shape {
    @Override
    public void draw(int length, int width, String fillStyle) {

    }

    @Override
    public void draw(int radius, String fillColor, String lineColor) {

    }
}
