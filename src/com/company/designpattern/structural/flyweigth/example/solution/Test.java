package com.company.designpattern.structural.flyweigth.example.solution;

/**
 * @author Mahendra Prajapati
 * @since 31-07-2020
 */
public class Test {
    public static void main(String[] args) {
        new PaintApp().render(10);
    }
}
