package com.company.designpattern.structural.flyweigth.example.solution;

/**
 * @author Mahendra Prajapati
 * @since 30-07-2020
 */
public class Retangle extends ShapeAdapter {

    private String label;
    private int length;
    private int width;
    private String fillStyle;

    public Retangle() {
        super();
        this.label = "Rectangle";
    }

    public String getLabel() {
        return label;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getFillStyle() {
        return fillStyle;
    }

    public void setFillStyle(String fillStyle) {
        this.fillStyle = fillStyle;
    }

    @Override
    public void draw(int length, int width, String fillStyle) {
        this.setFillStyle(fillStyle);
        this.setLength(length);
        this.setWidth(width);
        System.out.println(toString());
    }

    @Override
    public String toString() {
        return "Retangle{" +
                "label='" + getLabel() + '\'' +
                ", length=" + getLabel() +
                ", width=" + getWidth() +
                ", fillStyle='" + getFillStyle() + '\'' +
                ", hashcode='" + hashCode() + '\'' +
                '}';
    }
}
