package com.company.designpattern.structural.flyweigth.example.solution;

/**
 * @author Mahendra Prajapati
 * @since 31-07-2020
 */
public class PaintApp {
    public void render(int noOfShape) {
        Shape shape = null;

        for (int i = 0; i < noOfShape; i++) {
            if(i % 2 == 0) {
                shape = ShapeFactory.getShape("Circle");
                shape.draw(i, "Red", "Blue");
            } else {
                shape = ShapeFactory.getShape("Rectangle");
                shape.draw(i, 2 * i, "Blue");
            }
        }
    }
}
