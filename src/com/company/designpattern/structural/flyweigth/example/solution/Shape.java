package com.company.designpattern.structural.flyweigth.example.solution;

/**
 * @author Mahendra Prajapati
 * @since 30-07-2020
 */
public interface Shape {
    void draw(int length, int width, String fillStyle);
    void draw(int radius, String fillColor, String lineColor);
}
