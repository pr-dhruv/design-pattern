package com.company.designpattern.structural.flyweigth.example.solution;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Mahendra Prajapati
 * @since 31-07-2020
 */
public class ShapeFactory {

    private static Map<String, Shape> shapeMap = new HashMap<>();

    public static Shape getShape(String shapeType) {
        Shape shape = null;
        if(!shapeMap.containsKey(shapeType)){
            if(shapeType.equalsIgnoreCase("circle"))
                shape = new Circle();
            else if (shapeType.equalsIgnoreCase("rectangle"))
                shape = new Retangle();
            shapeMap.put(shapeType, shape);
        }
        return shapeMap.get(shapeType);
    }

}
