package com.company.designpattern.structural.flyweigth.example.problem;

/**
 * @author Mahendra Prajapati
 * @since 30-07-2020
 */
public class Retangle implements Shape {

    private String label;
    private int length;
    private int width;
    private String fillStyle;

    public Retangle() {
        super();
        this.label = "Rectangle";
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getFillStyle() {
        return fillStyle;
    }

    public void setFillStyle(String fillStyle) {
        this.fillStyle = fillStyle;
    }

    @Override
    public void draw() {
        System.out.println(toString());
    }

    @Override
    public String toString() {
        return "Retangle{" +
                "label='" + label + '\'' +
                ", length=" + length +
                ", width=" + width +
                ", fillStyle='" + fillStyle + '\'' +
                ", hashcode='" + hashCode() + '\'' +
                '}';
    }
}
