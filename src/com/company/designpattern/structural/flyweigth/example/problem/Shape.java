package com.company.designpattern.structural.flyweigth.example.problem;

/**
 * @author Mahendra Prajapati
 * @since 30-07-2020
 */
public interface Shape {
    void draw();
}
