package com.company.designpattern.structural.flyweigth.example.problem;

/**
 * @author Mahendra Prajapati
 * @since 30-07-2020
 */
public class Circle implements Shape  {

    private int radius;
    private String label;
    private String fillColor;
    private String lineColor;

    public Circle() {
        super();
        this.label = "Circle";
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getFillColor() {
        return fillColor;
    }

    public void setFillColor(String fillColor) {
        this.fillColor = fillColor;
    }

    public String getLineColor() {
        return lineColor;
    }

    public void setLineColor(String lineColor) {
        this.lineColor = lineColor;
    }

    @Override
    public void draw() {
        System.out.println(toString());
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", label='" + label + '\'' +
                ", fillColor='" + fillColor + '\'' +
                ", lineColor='" + lineColor + '\'' +
                ", hashcode='" + hashCode() + '\'' +
                '}';
    }
}
