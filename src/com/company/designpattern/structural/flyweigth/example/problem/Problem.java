package com.company.designpattern.structural.flyweigth.example.problem;

/**
 * @author Mahendra Prajapati
 * @since 31-07-2020
 */
public class Problem {
    public static void main(String[] args) {
        PaintApp app = new PaintApp();
        app.render(10);
    }
}
