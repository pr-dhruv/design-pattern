package com.company.designpattern.structural.flyweigth.example.problem;

/**
 * @author Mahendra Prajapati
 * @since 30-07-2020
 */
public class PaintApp {

    public void render(int noOfShape) {
        Shape[] shapes = new Shape[noOfShape];
        for (int i =0 ; i < noOfShape; i++) {
            if(i % 2 == 0) {
                shapes[i] = new Circle();
                ((Circle)shapes[i]).setRadius(i);
                ((Circle)shapes[i]).setFillColor("red");
                ((Circle)shapes[i]).setLineColor("white");
            } else {
                shapes[i] = new Retangle();
                ((Retangle)shapes[i]).setWidth(i);
                ((Retangle)shapes[i]).setLength(i+i);
                ((Retangle)shapes[i]).setFillStyle("dotted");
            }
            shapes[i].draw();
        }
    }
}
