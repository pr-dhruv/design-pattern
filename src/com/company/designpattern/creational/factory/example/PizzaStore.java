package com.company.designpattern.creational.factory.example;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */

public class PizzaStore {
    public Pizza orderPizza(String pizzaType) {
        Pizza pizza = PizzaFactory.createPizza(pizzaType);
        pizza.prepare().bake().cut();
        return pizza;
    }
}
