package com.company.designpattern.creational.factory.example;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class ChickenPizza implements Pizza {
    @Override
    public Pizza prepare() {
        System.out.println("Cheese Pizza Prepared");
        return this;
    }

    @Override
    public Pizza bake() {
        System.out.println("Cheese Pizza baked");
        return this;
    }

    @Override
    public Pizza cut() {
        System.out.println("Cheese Pizza cut");
        return this;
    }
}
