package com.company.designpattern.creational.factory.example;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class VegPizza implements Pizza {

    @Override
    public Pizza prepare() {
        System.out.println("Veg Pizza Prepared");
        return this;
    }

    @Override
    public Pizza bake() {
        System.out.println("Veg Pizza baked");
        return this;
    }

    @Override
    public Pizza cut() {
        System.out.println("Veg Pizza cut");
        return this;
    }
}
