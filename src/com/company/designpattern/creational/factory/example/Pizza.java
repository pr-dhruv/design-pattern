package com.company.designpattern.creational.factory.example;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public interface Pizza {
    Pizza prepare();

    Pizza bake();

    Pizza cut();
}
