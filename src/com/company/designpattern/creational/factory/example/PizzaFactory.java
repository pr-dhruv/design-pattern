package com.company.designpattern.creational.factory.example;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class PizzaFactory {

    public static Pizza createPizza(String pizzaType) {
        Pizza pizza = null;

        if (pizzaType.equalsIgnoreCase("veg"))
            pizza = new VegPizza();
        else if (pizzaType.equalsIgnoreCase("chicken"))
            pizza = new ChickenPizza();
        else if (pizzaType.equalsIgnoreCase("cheese"))
            pizza = new CheesePizza();

        return pizza;
    }

}
