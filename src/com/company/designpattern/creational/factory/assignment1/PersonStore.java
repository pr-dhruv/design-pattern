package com.company.designpattern.creational.factory.assignment1;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class PersonStore {
    public Person getPerson(String personType) {
        return PersonFactory.getPerson(personType);
    }
}
