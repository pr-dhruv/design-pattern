package com.company.designpattern.creational.factory.assignment1;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class Client {
    public static void main(String[] args) {
        PersonStore ps = new PersonStore();
        Person person = ps.getPerson("female");
        person.sayHello();
    }
}
