package com.company.designpattern.creational.factory.assignment1;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class PersonFactory {
    public static Person getPerson(String personType) {
        Person person = null;
        if(personType.equalsIgnoreCase("male"))
            person = new Male();
        else if(personType.equalsIgnoreCase("female"))
            person = new Female();
        return person;
    }
}
