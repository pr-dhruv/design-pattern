package com.company.designpattern.creational.factory.assignment1;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public interface Person {
    public void sayHello();
}
