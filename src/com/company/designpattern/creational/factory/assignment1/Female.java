package com.company.designpattern.creational.factory.assignment1;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class Female implements Person {
    @Override
    public void sayHello() {
        System.out.println("I am female");
    }
}
