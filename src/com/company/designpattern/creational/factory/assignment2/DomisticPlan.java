package com.company.designpattern.creational.factory.assignment2;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class DomisticPlan extends BasePlan {

    public DomisticPlan() {
        super();
        System.out.println("Domestic Plan");
        this.interestRate = 3.50;
    }

}
