package com.company.designpattern.creational.factory.assignment2;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class PlanFactory {
    public static Plan getPlan(String planType) {
        Plan plan = null;

        if(planType.equalsIgnoreCase("domestic"))
            plan = new DomisticPlan();
        else if(planType.equalsIgnoreCase("commercial"))
            plan = new CommercialPlan();
        else if(planType.equalsIgnoreCase("institute"))
            plan = new InstitutePlan();

        return plan;
    }
}
