package com.company.designpattern.creational.factory.assignment2;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public abstract class BasePlan implements Plan{
    protected double interestRate;

    public BasePlan() {
        super();
    }

    @Override
    public double getInterestRate() {
        return interestRate;
    }

    @Override
    public double getCurrentBill(double principal) {
        return interestRate * principal;
    }
}
