package com.company.designpattern.creational.factory.assignment2;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class Client {
    public static void main(String[] args) {
        BillGenerator bg = new BillGenerator();
        System.out.println(bg.getPlan("domestic").getCurrentBill(125.50));
        System.out.println(bg.getPlan("institute").getCurrentBill(125.50));
        System.out.println(bg.getPlan("commercial").getCurrentBill(125.50));
    }
}
