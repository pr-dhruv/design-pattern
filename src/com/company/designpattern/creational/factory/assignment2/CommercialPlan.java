package com.company.designpattern.creational.factory.assignment2;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class CommercialPlan extends BasePlan {

    public CommercialPlan() {
        super();
        System.out.println("Commercial Plan");
        this.interestRate = 6.50;
    }
    
}
