package com.company.designpattern.creational.factory.assignment2;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class InstitutePlan extends BasePlan {

    public InstitutePlan() {
        super();
        System.out.println("Institute Plan");
        this.interestRate = 2.50;
    }

}
