package com.company.designpattern.creational.factory.assignment2;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class BillGenerator {
    public Plan getPlan(String planType) {
        return PlanFactory.getPlan(planType);
    }
}
