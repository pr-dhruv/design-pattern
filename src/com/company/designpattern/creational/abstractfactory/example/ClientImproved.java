package com.company.designpattern.creational.abstractfactory.example;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public class ClientImproved {
    public static void main(String[] args) {
        Dao dao = ImprovedDaoFactoryProducer.getDao("db", "emp");
        dao.save();
    }
}
