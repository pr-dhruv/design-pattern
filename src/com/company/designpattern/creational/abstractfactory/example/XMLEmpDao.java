package com.company.designpattern.creational.abstractfactory.example;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public class XMLEmpDao implements Dao {
    @Override
    public void save() {
        System.out.println("Inside XMLEmpDao save()");
    }
}
