package com.company.designpattern.creational.abstractfactory.example;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public class Client {
    public static void main(String[] args) {
        DaoAbstractFactory daoAbstractFactory = null;
        daoAbstractFactory = DAOFactoryProducer.produce("db");
        daoAbstractFactory.getDao("emp").save();
        daoAbstractFactory.getDao("dept").save();

        daoAbstractFactory = DAOFactoryProducer.produce("xml");
        daoAbstractFactory.getDao("emp").save();
        daoAbstractFactory.getDao("dept").save();
    }
}
