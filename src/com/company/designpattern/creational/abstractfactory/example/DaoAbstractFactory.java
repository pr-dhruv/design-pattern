package com.company.designpattern.creational.abstractfactory.example;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public abstract class DaoAbstractFactory {
    public abstract Dao getDao(String daoType);
}
