package com.company.designpattern.creational.abstractfactory.example;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public class ImprovedDaoFactoryProducer {
    public static Dao getDao(String factoryType, String daoType) {
        Dao dao = null;

        if(factoryType.equalsIgnoreCase("db"))
            dao = new DBDaoFactory().getDao(daoType);
        else if(factoryType.equalsIgnoreCase("xml"))
            dao = new XMLDaoFactory().getDao(daoType);

        return dao;
    }
}
