package com.company.designpattern.creational.abstractfactory.example;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public class XMLDaoFactory extends DaoAbstractFactory {
    @Override
    public Dao getDao(String daoType) {
        Dao dao = null;

        if(daoType.equalsIgnoreCase("emp"))
            dao = new XMLEmpDao();
        else if(daoType.equalsIgnoreCase("dept"))
            dao = new XMLDeptDao();

        return dao;
    }
}
