package com.company.designpattern.creational.abstractfactory.example;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public class DAOFactoryProducer {
    public static DaoAbstractFactory produce(String factoryType) {
        DaoAbstractFactory daoAbstractFactory = null;

        if(factoryType.equalsIgnoreCase("xml"))
            daoAbstractFactory = new XMLDaoFactory();
        else if(factoryType.equalsIgnoreCase("db"))
            daoAbstractFactory = new DBDaoFactory();

        return daoAbstractFactory;
    }
}
