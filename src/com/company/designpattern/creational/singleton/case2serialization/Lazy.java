package com.company.designpattern.creational.singleton.case2serialization;

import java.io.Serializable;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class Lazy implements Serializable {
    private static final long serialVersionUID = 1L;
    private static Lazy lazy;

    private Lazy() {

    }

    public static Lazy getLazy() {
        if(lazy == null) {
            synchronized (Lazy.class) {
                if (lazy == null)
                    lazy = new Lazy();
            }
        }
        return lazy;
    }

    /**
     * Solution of Serialization
     *
     * It can be private, protected, public
     * @return Object
     */
    protected Object readResolve() {
        return lazy;
    }

}