package com.company.designpattern.creational.singleton.case2serialization;

import java.io.*;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class Problem {
    public static void main(String[] args) {
        try {
            Lazy lazy1 = Lazy.getLazy();
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File("Lazy1.txt")));
            oos.writeObject(lazy1);
            oos.close();

            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("Lazy1.txt")));
            Lazy lazy2 = (Lazy) ois.readObject();
            ois.close();

            System.out.println(lazy1 == lazy2);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
