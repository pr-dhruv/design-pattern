package com.company.designpattern.creational.singleton.case1threadsafe.solution1;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */

public class Lazy {
    private static Lazy lazy;

    private Lazy() {

    }

    public static synchronized Lazy getLazy() {
        if(lazy == null)
            lazy = new Lazy();
        return lazy;
    }
}