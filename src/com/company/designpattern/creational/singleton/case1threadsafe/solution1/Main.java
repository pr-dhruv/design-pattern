package com.company.designpattern.creational.singleton.case1threadsafe.solution1;

public class Main {
    public static void main(String[] args) {
        Thread t1 = new Thread(() -> System.out.println(Lazy.getLazy().toString()));

        Thread t2 = new Thread(() -> System.out.println(Lazy.getLazy().toString()));

        t1.start();
        t2.start();

    }
}