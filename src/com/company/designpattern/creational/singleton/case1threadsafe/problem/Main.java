package com.company.designpattern.creational.singleton.case1threadsafe.problem;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */

/**
 * Because of the system speed we won't be able to see the different ID of the threds.
 */
public class Main {
    public static void main(String[] args) {
        Thread t1 = new Thread(() -> System.out.println(Lazy.getLazy().toString()));

        Thread t2 = new Thread(() -> System.out.println(Lazy.getLazy().toString()));

        t1.start();
        t2.start();

    }
}
