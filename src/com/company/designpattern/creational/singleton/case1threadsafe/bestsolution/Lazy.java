package com.company.designpattern.creational.singleton.case1threadsafe.bestsolution;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class Lazy {
    private static volatile Lazy lazy;

    private Lazy() {

    }

    public static Lazy getLazy() {
        if(lazy == null) {
            synchronized (Lazy.class) {
                if (lazy == null)
                    lazy = new Lazy();
            }
        }
        return lazy;
    }
}
