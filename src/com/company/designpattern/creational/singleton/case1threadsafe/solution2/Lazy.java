package com.company.designpattern.creational.singleton.case1threadsafe.solution2;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class Lazy {
    private static Lazy lazy;

    private Lazy() {

    }

    public static Lazy getLazy() {
        synchronized (Lazy.class) {
            if (lazy == null)
                lazy = new Lazy();
        }
        return lazy;
    }
}
