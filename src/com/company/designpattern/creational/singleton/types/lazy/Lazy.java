package com.company.designpattern.creational.singleton.types.lazy;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class Lazy {
    private static Lazy lazy;

    private Lazy() {

    }

    public static Lazy getLazy() {
        if(lazy == null)
            lazy = new Lazy();
        return lazy;
    }
}
