package com.company.designpattern.creational.singleton.types.lazy;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class LazyMain {
    public static void main(String[] args) {
        Lazy instance1 = Lazy.getLazy();
        Lazy instance2 = Lazy.getLazy();

        System.out.println(instance1 == instance2);
    }
}
