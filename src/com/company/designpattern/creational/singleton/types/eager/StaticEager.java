package com.company.designpattern.creational.singleton.types.eager;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class StaticEager {
    private static StaticEager staticEager;

    static {
        staticEager = new StaticEager();
    }

    private StaticEager() {

    }

    public static StaticEager getStaticEager() {
        return staticEager;
    }
}
