package com.company.designpattern.creational.singleton.types.eager;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class Eager {
    private static Eager eager = new Eager();

    private Eager() {

    }

    public static Eager getEager() {
        return eager;
    }
}
