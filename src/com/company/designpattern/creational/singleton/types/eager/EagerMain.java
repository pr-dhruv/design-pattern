package com.company.designpattern.creational.singleton.types.eager;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class EagerMain {
    public static void main(String[] args) {
        Eager instance1 = Eager.getEager();
        Eager instance2 = Eager.getEager();

        System.out.println(instance1 == instance2);
    }
}
