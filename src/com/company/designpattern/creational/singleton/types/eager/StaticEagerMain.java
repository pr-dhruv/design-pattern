package com.company.designpattern.creational.singleton.types.eager;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class StaticEagerMain {
    public static void main(String[] args) {
        StaticEager instance1 = StaticEager.getStaticEager();
        StaticEager instance2 = StaticEager.getStaticEager();

        System.out.println(instance1 == instance2);
    }
}
