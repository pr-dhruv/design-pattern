package com.company.designpattern.creational.singleton.case3clonable;

import java.io.Serializable;

/**
 * @author Mahendra Prajapati
 * @since 25-07-2020
 */
public class Lazy implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;
    private static Lazy lazy;

    private Lazy() {

    }

    public static Lazy getLazy() {
        if(lazy == null) {
            synchronized (Lazy.class) {
                if (lazy == null)
                    lazy = new Lazy();
            }
        }
        return lazy;
    }

    /**
     * Solution of Serialization
     *
     * It can be private, protected, public
     * @return Object
     */
    protected Object readResolve() {
        return lazy;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        // solution 1
        //throw new CloneNotSupportedException();

        // Solution 2
        return lazy;
    }
}