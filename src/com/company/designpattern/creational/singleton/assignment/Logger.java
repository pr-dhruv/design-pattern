    package com.company.designpattern.creational.singleton.assignment;


    import java.io.Serializable;

    /**
     * @author Mahendra Prajapati
     * @since 25-07-2020
     */
    public class Logger implements Serializable, Cloneable {

        private static final long serialVersionUID = 1L;
        private static volatile Logger log;
        private Logger() {

        }

        public static Logger getLog() {
            if(log == null) {
                synchronized (Logger.class) {
                    if(log == null)
                        log = new Logger();
                }
            }
            return log;
        }

        @Override
        public Object clone() throws CloneNotSupportedException {
            throw new CloneNotSupportedException("Cloning not supported");
        }

        protected Object readResolve() {
            return log;
        }

        public void info(String message) {
            System.out.println(message);
        }
    }
