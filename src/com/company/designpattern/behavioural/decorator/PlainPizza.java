package com.company.designpattern.behavioural.decorator;

/**
 * @author Mahendra Prajapati
 * @since 01-08-2020
 */
public class PlainPizza implements Pizza {
    @Override
    public void bake() {
        System.out.println("Baking plain Pizza");
    }
}
