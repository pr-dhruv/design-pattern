package com.company.designpattern.behavioural.decorator;

/**
 * @author Mahendra Prajapati
 * @since 01-08-2020
 */
public class VegPizza extends PizzaDecorator {

    public  VegPizza() {
        super();
    }

    public VegPizza(Pizza pizza) {
        super(pizza);
        System.out.println("Adding veg toppings");
    }

}
