package com.company.designpattern.behavioural.decorator;

/**
 * @author Mahendra Prajapati
 * @since 01-08-2020
 */
public class Client {
    public static void main(String[] args) {
        Pizza pizza = new VegPizza(new PlainPizza());
        pizza.bake();

        Pizza pizza1 = new ChickenPizza(new VegPizza(new PlainPizza()));
        pizza.bake();
    }
}
