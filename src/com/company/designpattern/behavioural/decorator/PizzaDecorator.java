package com.company.designpattern.behavioural.decorator;

/**
 * @author Mahendra Prajapati
 * @since 01-08-2020
 */
public class PizzaDecorator implements Pizza {

    private Pizza pizza;

    public PizzaDecorator() {
        super();
    }

    public PizzaDecorator(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public void bake() {
        pizza.bake();
    }
}
