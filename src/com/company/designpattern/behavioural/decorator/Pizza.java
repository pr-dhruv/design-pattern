package com.company.designpattern.behavioural.decorator;

/**
 * @author Mahendra Prajapati
 * @since 01-08-2020
 */
public interface Pizza {
    void bake();
}
