package com.company.designpattern.behavioural.decorator;

/**
 * @author Mahendra Prajapati
 * @since 01-08-2020
 */
public class ChickenPizza extends PizzaDecorator {

    public ChickenPizza() {
        super();
    }

    public ChickenPizza(Pizza pizza) {
        super(pizza);
        System.out.println("Adding Chicken toppings");
    }
}
