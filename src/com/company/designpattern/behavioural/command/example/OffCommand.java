package com.company.designpattern.behavioural.command.example;

/**
 * @author Mahendra Prajapati
 * @since 01-08-2020
 */
public class OffCommand implements Command {
    private Television television;

    public OffCommand() {
        super();
    }

    public OffCommand(Television television) {
        this.television = television;
    }

    @Override
    public void execute() {
        television.off();
    }
}
