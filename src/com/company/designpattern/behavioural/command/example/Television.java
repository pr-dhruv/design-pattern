package com.company.designpattern.behavioural.command.example;

/**
 * @author Mahendra Prajapati
 * @since 01-08-2020
 */
public class Television {
    public void on() {
        System.out.println("Switched On");
    }

    public void off() {
        System.out.println("Switched Off");
    }

}
