package com.company.designpattern.behavioural.command.example;

/**
 * @author Mahendra Prajapati
 * @since 01-08-2020
 */
public class Client {

    public static void main(String[] args) {
        Television television  = new Television();
        RemoteController remoteController = new RemoteController();
        Command command = new OnCommand(television);
        remoteController.setCommand(command);
        remoteController.pressButton();

        command =  new OffCommand(television);
        remoteController.setCommand(command);
        remoteController.pressButton();
    }

}
