package com.company.designpattern.behavioural.command.example;

/**
 * @author Mahendra Prajapati
 * @since 01-08-2020
 */
public class OnCommand implements Command {
    private Television television;

    public OnCommand() {
        super();
    }

    public OnCommand(Television television) {
        this.television = television;
    }

    @Override
    public void execute() {
        television.on();
    }
}
