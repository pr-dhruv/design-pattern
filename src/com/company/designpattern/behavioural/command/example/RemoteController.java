package com.company.designpattern.behavioural.command.example;

/**
 * @author Mahendra Prajapati
 * @since 01-08-2020
 */
public class RemoteController {

    private Command command;

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

    public void pressButton() {
        command.execute();
    }
}
