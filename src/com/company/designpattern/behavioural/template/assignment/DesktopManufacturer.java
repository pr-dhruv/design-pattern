package com.company.designpattern.behavioural.template.assignment;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public class DesktopManufacturer extends ComputerManufacturer{
    @Override
    public void addHardDrive() {
        System.out.println("\tAdding 3.5 inch Hard drive");
    }

    @Override
    public void addRam() {
        System.out.println("\tAdding full sized RAM");
    }

    @Override
    public void addKeyboard() {
        System.out.println("\tAdding full sized external keyboard");
    }
}
