package com.company.designpattern.behavioural.template.assignment;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public abstract class ComputerManufacturer {

    public void buildComputer() {
        System.out.println("Building Computer : ");
        addHardDrive();
        addRam();
        addKeyboard();
    }

    public abstract void addHardDrive();

    public abstract void addRam();

    public abstract void addKeyboard();

}
