package com.company.designpattern.behavioural.template.assignment;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public class ComputerShop {
    public static void main(String[] args) {
        ComputerManufacturer computerManufacturer = new DesktopManufacturer();
        computerManufacturer.buildComputer();

        computerManufacturer = new LaptopManufacturer();
        computerManufacturer.buildComputer();
    }
}
