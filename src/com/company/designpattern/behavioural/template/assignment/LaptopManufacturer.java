package com.company.designpattern.behavioural.template.assignment;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public class LaptopManufacturer extends ComputerManufacturer {
    @Override
    public void addHardDrive() {
        System.out.println("\tAdding 2.5 inch Hard drive");
    }

    @Override
    public void addRam() {
        System.out.println("\tAdding small sized RAM");
    }

    @Override
    public void addKeyboard() {
        System.out.println("\tAdding in-built keyboard");
    }
}
