package com.company.designpattern.behavioural.template.example;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public class JSONDataRenderer extends DataRenderer {
    @Override
    public String readData() {
        return "JSON Data";
    }

    @Override
    public String processData(String data) {
        return "Processed : " + data;
    }
}
