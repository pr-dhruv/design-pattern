package com.company.designpattern.behavioural.template.example;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public abstract class DataRenderer {

    public void render() {
        System.out.println(processData(readData()));
    }

    public abstract String readData();

    public abstract String processData(String data);

}
