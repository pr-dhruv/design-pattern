package com.company.designpattern.behavioural.template.example;

/**
 * @author Mahendra Prajapati
 * @since 26-07-2020
 */
public class UIClient {
    public static void main(String[] args) {
        DataRenderer renderer = new JSONDataRenderer();
        renderer.render();

        renderer = new XmlDataRenderer();
        renderer.render();
    }
}
